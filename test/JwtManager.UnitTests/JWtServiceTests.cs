using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FluentAssertions;
using Microsoft.IdentityModel.Tokens;

namespace JwtManager.UnitTests;

public class JWtServiceTests
{
    [Fact]
    public void Given_Empty_Claims_It_Should_Return_Valid_Jwt()
    {
        // Given
        var jwtService = new JwtService();

        // When
        var result = 
            jwtService.CreateJwt(
                "este es un secreto mas largo", 
                Enumerable.Empty<Claim>());

        // Then
        result.Should().NotBeEmpty();
    }

    [Fact]
    public void Given_Empty_Secret_It_Should_Throw_EmptySecretException()
    {
        // Given
        var jwtService = new JwtService();
        Exception exception = null!;

        // When
        try
        {
            jwtService.CreateJwt(string.Empty, Enumerable.Empty<Claim>());
        }
        catch (EmptySecretException ex)
        {
            exception = ex;
        }
        

        // Then
        exception.Should().NotBeNull();
    }

    [Fact]
    public void Given_Secret_It_Should_Be_Signed_With_That_Secret()
    {
        // Given
        var jwtService = new JwtService();
        var secret = "this is my sample secret";
        var expectedSecret = "this is my sample secret";

        // When
        var result = jwtService.CreateJwt(secret, Enumerable.Empty<Claim>());

        // Then
        var isSignedWithSameKey = IsValidSignature(result, expectedSecret);
        isSignedWithSameKey.Should().BeTrue();
    }

    private bool IsValidSignature(string jwt, string secret)
    {
        var key = Encoding.ASCII.GetBytes(secret);
        var securityKey = new SymmetricSecurityKey(key);

        var tokenHandler = new JwtSecurityTokenHandler();
        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateAudience = false,
            ValidateIssuer = false,
            IssuerSigningKey = securityKey
        };

        try
        {
            _ = tokenHandler.ValidateToken(jwt, validationParameters, out _);
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }
}