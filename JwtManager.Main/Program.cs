﻿using System.Security.Claims;
using JwtManager;

var jwtService = new JwtService();

var claims =
    new List<Claim>
    {
        new Claim("name", "Pepe"),
        new Claim("email", "pepe@test.com"),
        new Claim("is_vip", true.ToString(), ClaimValueTypes.Boolean)
    };

var jwt = jwtService.CreateJwt("this is my fancy secret that only I know", claims);

Console.WriteLine(jwt);