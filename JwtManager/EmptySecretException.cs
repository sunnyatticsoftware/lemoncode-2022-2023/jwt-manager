﻿namespace JwtManager;
public class EmptySecretException
    : Exception
{
    public EmptySecretException()
        : base("Secret is empty")
    {
    }
}
