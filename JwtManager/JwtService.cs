﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace JwtManager;
public class JwtService
{
    public string CreateJwt(string secret, IEnumerable<Claim> claims)
    {
        if (string.IsNullOrWhiteSpace(secret))
        {
            throw new EmptySecretException();
        }

        var key = Encoding.ASCII.GetBytes(secret);
        var securityKey = new SymmetricSecurityKey(key);

        var currentTime = DateTime.UtcNow;
        var expirationTime = currentTime.AddSeconds(3600);
        var tokenHandler = new JwtSecurityTokenHandler();

        var tokenDescriptor =
            new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                IssuedAt = currentTime,
                NotBefore = currentTime,
                Expires = expirationTime,
                Issuer = "lemoncode",
                Audience = "sample",
                SigningCredentials =
                    new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        var result = tokenHandler.WriteToken(token) 
                     ?? throw new CryptographicException("Could not create JWT");
        return result;
    }
}
